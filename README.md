INTRODUCTION
------------
Events is a modern and versatile Drupal theme designed to host events of all sizes. Whether you're organizing conferences, seminars, workshops, or social gatherings, our theme provides the perfect platform to effectively promote and manage your events.


THEME FEATURES
--------------

Drupal 9.x compatible
Configurable slider for front page
Configurable Footer Icons
Configurable slide-up button
Dedicated speakers and event-listing Section
Responsive layout
A total of 6 regions
Owl carousel Foe NewsFeeds Section
Styled links-buttons
Use of Font Awesome icons
Use of wow.js Effects
Minimal design and nice typography


THEME REGIONS
-------------

Header
Banner
Content
Left sidebar
Right sidebar
Footer First
Disbaled

CONFIGURATION
-------------
Navigate to: Administration >> Appearance >> Settings >> events
